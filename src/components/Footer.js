import React from 'react';

import { AppBar, Toolbar, Typography, Button, MenuItem, Grid } from '@material-ui/core'
import Menu from './Menu'
import logo from '../assets/logo.jpg'

function Chip(props) {
    return (
        <div className='chip'>
            <p className='chipText'>
                {props.text}
            </p>
        </div>
    )
}
function Vidoes(props) {
    return (
        <div className='videoContainer'>
            <video width="120" height="90" controls preload="none">
                <source src={props.url} type="video/mp4" />
            </video>
        </div>
    )
}

function App() {
    return (
        <div className='footer'>
            <div className='footerUpper'>
                <Grid className='footerLeft'>
                    <div style={{
                        backgroundColor: '#fff',
                        borderRadius: 70,
                        height: 140,
                        width: 140

                    }}>
                        <img style={{
                            width: 100,
                            borderRadius: 50,
                            paddingLeft: 4,
                            paddingTop: 12
                        }} src={logo} />
                    </div>
                    <div>
                        <p className='white'>
                            We are a group of people working together for environmental justice and freedom from toxics. We have taken it upon ourselves to collect and share information...
                        </p>
                    </div>
                    <div>
                        <Chip text="+91-011-23233519, 23214731" />
                        <Chip text="info@coeh.org.in" />

                    </div>
                </Grid>
                <Grid className='footerRight'>
                    <div className='footerHeading'>
                        <h2 className='white'>
                            VIDEO GALLERY
    </h2>
                    </div>

                    <div className='footerRightChild'>
                        <div className='footerRightBlock' >

                            <div className='frame'>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/oILtVFvzKa8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/9c618QGKNPc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/Zn3whsSCz_0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                            </div>
                        </div>
                        <div className='footerRightBlock'>


                            <div className='frame'>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/sgonEC0iDfk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/dMsout4Rkvg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/XT5Q_bFxGdg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



                            </div>
                        </div>
                        <div className='footerRightBlock'>

                            <div className='frame'>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/QY4fQTqh0mk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/gTLTHJev_LY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe className="padding30" width="160" height="120" src="https://www.youtube.com/embed/PvcZRHwvcbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                            </div>
                        </div>

                    </div>

                </Grid>

            </div>
            <div className='redBack' style={{ minHeight: "50px", boxShadow: 'none' }} position="static" />

        </div>
    );
}

export default App;
