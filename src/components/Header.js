import React, { useState } from 'react';

import { AppBar, Toolbar, Typography, Button, MenuItem, Grid } from '@material-ui/core'
import Menu from './Menu'
import Menus from './MenuData'

class MenuBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isStateChanged: false
        }
    }
    reRenderHeader = () => {
        console.log("changeing state ============>>>>>")
        this.setState({ isStateChanged: !this.state.isStateChanged });
    }
    render() {
        return (
            <div>
                <AppBar style={{ height: "75px", boxShadow: 'none', backgroundColor: "#3d86b6" }} position="static">


                    <div style={{ width: "300px", height: "75px", backgroundColor: "#000", zIndex: "1000000000" }} />

 

                    <div style={{margin:'0 auto'}}>
                        <Grid container direction='row' style={{ backgroundColor: "transparent", height: "64px", marginTop: "15px" }}>



                            <div style={{ width: "150px", height: "102px", paddingTop: 20, backgroundColor: "#fff", borderTopLeftRadius: "10px", borderTopRightRadius: "10px", borderBottom: '2px #eb1b22 solid' }}>
                                <img style={{ width: '70px', height: "70px", marginTop: '10px' }} src={require('../assets/logo.jpg')} />
                            </div>

                            <div className="menubar">
                                <div style={{ height: "30px", backgroundColor: "transparent", minWidth: "900px", width: "100%" }} />


                                <div className='menus' style={{ height: "92px", backgroundColor: "#fff", width: "100%", borderBottom: '2px #3d86b6   solid' }} >
                                    {
                                        Menus.map((menu, index) =>
                                            <Menu setStateChange={this.reRenderHeader} key={index} menu={menu} />
                                        )
                                    }

                                </div>


                            </div>


                        </Grid>

                    </div>

                </AppBar>
            </div>
        );
    }

}

export default MenuBar;
