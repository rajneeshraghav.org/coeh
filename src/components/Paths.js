export const Messages = "/details/mesages";
export const Board = "/details/board";
export const Profile = "/details/profile";
export const Team = "/details/team";
export const Journey = "/details/journey";
export const Secretary = "/details/secretary";
export const Afih = "/details/afih";
export const Home = "/";
export const message = "/details/mesages";

export const Conference = "/details/conference";
export const Workshops = "/details/workshops";
export const seminars = "/details/seminars";

export const PlasticWasteManagemeng = "/details/PlasticWasteManagemeng";
export const MunicipalWasteManagemeng = "/details/MunicipalWasteManagemeng";
export const HazardousWasteManagemeng = "/details/HazardousWasteManagemeng";
export const BioWasteManagemeng = "/details/BioWasteManagemeng";
export const eWasteManagemeng = "/details/eWasteManagemeng";
export const Policies = "/details/Policies";

export const Research = "/details/research";
export const Publications = "/details/Publications";
export const ContactUs = "/details/ContactUs";
export const Gallery = "/details/Gallery";

