import React, { useState } from 'react';
import { withRouter } from "react-router-dom";
import { Button, Menu, MenuItem, Fade } from '@material-ui/core'
import { makeStyles, createStyles } from '@material-ui/core/styles';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import { Popover } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    root: {
        backgroundColor: 'transparent',
        pointerEvents: 'none'
    },
}));

function CustomMenu(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [isMenuOpen, openMenu] = useState(false);
    const [menuStatus, setMenuStatus] = useState('inactive');
   
    const handleClick = (event) => {
        props.setStateChange(true);
        if (props.menu.subMenus.length > 0) {
            setAnchorEl(event.currentTarget);
            openMenu(true)
        } else if (props.menu.pathName) {
            props.history.push({ pathname: props.menu.pathName });
        }
    };

    const handleHover = (event) => {
        setTimeout(function () { handleClose() }, 1500);
        console.log("moving in")
        props.setStateChange(true);
        setMenuStatus('active');
        if (props.menu.subMenus.length > 0) {
            setAnchorEl(event.currentTarget);
            openMenu(true)
        }
    };
    const handleClose = () => {
        setAnchorEl(null);
        openMenu(false)
        setMenuStatus('inactive')
    };

    const handleSubMenu = (event, link) => {
        event.currentTarget = null;
        setAnchorEl(event.currentTarget);
        handleMouseOut();
        if (link) {
            props.history.push({ pathname: link });
        } else {
            this.handleClose();
        } 
    }
    const handleMouseOut = () => {
        console.log("moving out")
        props.setStateChange(true);
        setMenuStatus('inactive');

        setAnchorEl(null);
        openMenu(false)
    }

    return (
        <div onMouseLeave={handleMouseOut} onMouseEnter={handleHover} onClick={handleClick} className={`MenuBackground-${menuStatus}`} style={{ paddingTop: 32, minWidth: "75px", width: "auto", maxWidth: "110" }} >
            <div>
                <Button

                    aria-owns={isMenuOpen ? 'simple-menu' : null}
                    aria-haspopup={isMenuOpen ? "true" : "false"}
                    className={classes.root}
                    //aria-controls="simple-menu"
                    style={{ backgroundColor: 'transparent !important', color: menuStatus == 'active' ? "#fff" : '#000' }}>
                    {props.menu.menuName}
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    //keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}

                >
                    {isMenuOpen && props.menu.subMenus && props.menu.subMenus.map((menu, index) =>
                        <MenuItem key={index} onClick={(e) => {

                            handleSubMenu(e, menu.link)
                        }}>{menu.name}</MenuItem>
                    )}
                </Menu>
            </div>
        </div>
    );
}

export default withRouter(CustomMenu);
