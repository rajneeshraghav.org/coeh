import React, { Component } from 'react';
import logo from '../assets/sheila.jpg';
import daga from '../assets/drdagaonstage.jpg';
import "./Slider.css";
class CustomCarousel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imageUrl: null,
            nextUrl: null,
            index: 0

        }
    }
    componentDidMount() {
        let images = [
            'https://i.picsum.photos/id/866/800/800.jpg',
            'https://i.picsum.photos/id/237/800/800.jpg',
            'https://i.picsum.photos/id/866/800/800.jpg',
            'https://i.picsum.photos/id/237/800/800.jpg'
        ]
        console.log('hello')
        setTimeout(() => {
            if (this.state.index < images.length) {
                this.setState({ index: this.state.index + 1 }, () => {
                    this.setState({ imageUrl: images[this.state.index] })
                })

            } else {
                this.setState({
                    index: 0
                })
            } 
        }, 2000)

    }
    render() {
        return (
            <React.Fragment>
 

                <div className="slider-wrapper_Featured">
                    {this.state.imageUrl ?

                        <div>
                            <img style={{ width: "100%", height: "100%", maxHeight: "700px" }} src={require('../assets/drdagaonstage.jpg')} />

 
                        </div>


                        :
                        <img style={{ width: "100%", height: "100%" }} src={require('../assets/image.jpg')} />

                    }



                </div>






            </React.Fragment>
        );
    }
};
export default CustomCarousel;