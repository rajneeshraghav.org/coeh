import React from 'react';
import { Paper, Grid } from '@material-ui/core'



function App(props) {
    return (
        <div style={{ width: "100%", backgroundColor: "transparent", justifyContent: 'center' }}>
            <div style={{ maxWidth: 800, margin: "0 auto" }}>
                <h1>
                    <span style={{ fontSize: 40, color: '#3c84b2' }}>
                        {props.text1}
                    </span>
                    <span style={{ fontSize: 40, color: '#ae0d00', paddingLeft: 20 }}>
                        {props.text2}
                    </span></h1>
            </div>




        </div>
    );
}

export default App;
