import * as paths from './Paths';
const munesData = [
    {
        menuName: "Home",
        pathName: paths.Home,
        subMenus:
            []
    },
    {
        menuName: "About Us",
        subMenus:
            [{ name: "Messages", link: paths.Messages },
           // { name: "Board", link: paths.Board },
            { name: "Profile", link: paths.Profile },
            { name: "Team", link: paths.Team },
            { name: "Journey of Centre", link: paths.Journey },
            { name: "Founder Member Secretary", link: paths.Secretary }]
    },
    {
        menuName: "Courses",
        subMenus:
            [{ name: "Associate Fellow of Industial Health", link: paths.Afih }]
    },
    {
        menuName: "Events",
        subMenus:
            [{ name: "International Conference", link: paths.Conference },
            { name: "Workshops", link: paths.Workshops },
            { name: "Symposiums/Seminars", link: paths.seminars }]
    },
    {
        menuName: "Research",
        pathName:paths.Research,
        subMenus:
            []
    },
    {
        menuName: "Policies",
        pathName: paths.Policies,
        subMenus:
            [
                //     { name: "Plastic Waste Management", link: paths.PlasticWasteManagemeng },
                // { name: "Municipal Waste Management", link: paths.MunicipalWasteManagemeng },
                // { name: "Hazardous Waste Management", link: paths.HazardousWasteManagemeng },
                // { name: "Bio Medical Waste", link: paths.BioWasteManagemeng },
                // { name: "E-Waste", link: paths.eWasteManagemeng }
            ]
    }, 
    {
        menuName: "Publications",
        pathName: paths.Publications,
        subMenus:
            []
    },
    {
        menuName: "Gallery",
        pathName:paths.Gallery,
        subMenus:
            []
    },
    {
        menuName: "Contactus",
        pathName: paths.ContactUs,
        subMenus:
            []
    }
];

export default munesData;