import React from 'react';
import { Paper, Grid } from '@material-ui/core'
import Photos from './Phot'



function App() {
    return (
        <div style={{ width: "100%", backgroundColor: "#fff", justifyContent: 'center'}}>
            <div style={{ maxWidth: 800, margin: "0 auto" }}>
                <h1>
                    <span style={{ fontSize: 40, color: '#3c84b2' }}>
                        OUR
              </span>
                    <span style={{ fontSize: 40, color: '#ae0d00', paddingLeft: 20 }}>
                        GALLERY
                  </span></h1>


            </div>
            <Photos />



        </div>
    );
}

export default App;
