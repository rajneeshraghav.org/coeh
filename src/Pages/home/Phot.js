import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

//import tileData from './tileData';
import image0 from '../../assets/0.jpg'
import image1 from '../../assets/1.jpg'
import image2 from '../../assets/2.jpg'
import image3 from '../../assets/3.jpg'
import image4 from '../../assets/4.jpg'
import image5 from '../../assets/5.jpg'
import image6 from '../../assets/6.jpg'
import image7 from '../../assets/7.jpg'
import image8 from '../../assets/8.jpg'
import image9 from '../../assets/9.jpg'

const tileData1 = [
  {
    img: image0,
    title: '',
    author: 'author',
  },
  {
    img: image1,
    title: '',
    author: 'author',
  }, {
    img: image2,
    title: '',
    author: 'author',
  }, {
    img: image3,
    title: '',
    author: 'author',
  },
  {
    img: image4,
    title: '',
    author: 'author',
  }

];
const tileData2 = [
    {
    img: image5,
    title: '',
    author: 'author',
  }, {
    img: image6,
    title: '',
    author: 'author',
  }, {
    img: image7,
    title: '',
    author: 'author',
  },
  {
    img: image8,
    title: '',
    author: 'author',
  },
  {
    img: image9,
    title: '',
    author: 'author',
  }

];
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
}));

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *

 */
export default function SingleLineGridList() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <GridList className={classes.gridList} cols={2.5}>
        {tileData1.map((tile) => (
          <GridListTile key={tile.img}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}

            />
          </GridListTile>
        ))}
      </GridList>
      <GridList className={classes.gridList} cols={2.5}>
        {tileData2.map((tile) => (
          <GridListTile key={tile.img}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}

            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}
