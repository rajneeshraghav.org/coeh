import React from 'react';
import { Paper, Grid } from '@material-ui/core'



function App() {
    return (
        <div style={{ width: "100%", backgroundColor: "#fff", justifyContent: 'center',paddingTop:50 }}>
            <div style={{ maxWidth: 800, margin: "0 auto" }}>
                <p style={{ color: "#777777", fontSize: 20 }}>Our Mission & Our Programs</p>
                <h1>
                    <span style={{ fontSize: 40, color: '#3c84b2' }}>
                        WELCOME TO
              </span>
                    <span style={{ fontSize: 40, color: '#ae0d00', paddingLeft: 20 }}>
                        COEH
                  </span></h1>
                <p style={{ color: "#777777" }}>
                    We are a group of people working together for environmental justice and freedom from toxins. We have taken it upon ourselves to collect and share information about the sources and dangers of poisons in our environment and bodies, as well as clean and sustainable alternatives for India and the rest of the world.
      </p>

                <p>
                    <span style={{ fontSize: 30, color: '#777777' }}>
                        Our Working Areas
              </span>
                </p>
            </div>




            <div style={{ width: "100%" }}>
                <div style={{ margin: "0 auto" }}>
                    <Grid justify='center' container>
                        <div style={{ width: 250, height: 250, padding: 20 }}>
                            <Paper style={{ padding: 20 }}>
                                <h4>
                                    Chemicals & Health

          </h4>
                                <p>
                                    Chemicals have become an integral part of our life and are linked to the development.
          </p>
                            </Paper>
                        </div>

                        <div style={{ width: 250, height: 250, padding: 20 }}>
                            <Paper style={{ padding: 20, height: 160 }}>
                                <h4>

                                    Waste & Sustainability

          </h4>
                                <p>
                                    Sustained economic growth and lifestyle changes fuels incremental change
          </p>
                            </Paper>
                        </div>


                        <div style={{ width: 250, height: 250, padding: 20 }}>
                            <Paper style={{ padding: 20, height: 160 }}>
                                <h4>
                                    Green Initiatives

          </h4>
                                <p>
                                    The Green Initiative has as its main objective the offsetting of Greenhouse
          </p>
                            </Paper>
                        </div>



                        <div style={{ width: 250, height: 250, padding: 20 }}>
                            <Paper style={{ padding: 20, height: 160 }}>
                                <h4>

                                    Other Activities

          </h4>
                                <p>
                                   COEH Link did a study on many other programers areas of different issues..
          </p>
                            </Paper>
                        </div>

                    </Grid>

                </div>

            </div>
        </div>
    );
}

export default App;
