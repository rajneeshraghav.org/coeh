import React from 'react';
import { Paper, Typography, Grid } from '@material-ui/core'


function App() {
    return (
        <React.Fragment>
            <div style={{ width: "100%", backgroundColor: "#3d86b6", height: 250, paddingTop: 15 }}>
                <div className='strip'>
                    <div className="stripContent">
                        <Typography style={{ fontSize: 25, textAlign: "left",color:'#fff', padding: 20 }}>
                            Health Impact of Firecrackers Bursting during Diwali in National Capital of
                            Delhi: Study awarded and funded by Central Pollution Control Board to study
                            short, intermediate and long – term health effects due to fire – crackers during
                            Diwali festival for five consequent years...

 </Typography>
                    </div>
                    <div>
                        <img style={{ height: 200, width: 250 }} src={require('../../assets/fire.jpg')} />

                    </div>

                </div>

            </div>

            <div style={{ width: "100%", backgroundColor: "#eb1b22", paddingTop: 15 }}>
                <div className='strip'>
                    <div className="stripContent">
                        <Typography style={{ fontSize: 25, color:'#fff',textAlign: "left", padding: 20 }}>
                            A tripartite study awarded and funded by Indian Oil Corporation
                            Limited. Aim of the study is to assess the adverse health effects caused by
                            vehicular emissions & ambient air quality monitoring. Study was based on
                            Government of India’s (MoP&NG) clean air initiative which includes major
                            infrastructure ...
 </Typography>
                    </div>
                    <div>
                        <img style={{ height: 200, width: 250 }} src={require('../../assets/auto.jpg')} />

                    </div>

                </div>
            </div>


            <div style={{ width: "100%", backgroundColor: "#3d86b6", paddingTop: 15 }}>
                <div className='strip'>
                    <div className="stripContent">
                        <Typography style={{ fontSize: 25,color:'#fff', textAlign: "left", padding: 20 }}>
                            In the light of the present study, the results showed that that the main risk for human health may arise from chronic inhalation exposure. Out of total 557 study subjects, maximum no. of individuals were reported with prevalence of Breathing Difficulty (28.2%), and 20.30 % with eye disorders...

 </Typography>
                    </div>
                    <div>
                        <img style={{ height: 200, width: 250 }} src={require('../../assets/solapur.jpeg')} />

                    </div>

                </div>
            </div>

        </React.Fragment>
    );
}

export default App;
