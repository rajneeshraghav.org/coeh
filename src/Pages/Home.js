import React from 'react';
import { Paper, Typography, Grid } from '@material-ui/core'
import Header from '../components/Header';
import Carousel from '../components/Carousel'
import Welcome from './home/Welcome'
import Campains from './home/Campains'
import Latest from './home/Latest';
import News from './home/News';
import Gallary from './home/Gallary';
import Footer from '../components/Footer'
import Strip from './home/Strips'


function App() {
  return (
    <div>
      
      <Carousel />
      <Strip />
      <Welcome />
      <News />
      <Latest />
   
      <Gallary />
  
     

    </div>

  );
}

export default App;
