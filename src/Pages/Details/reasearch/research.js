import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>
            <h5>
                EHS Management in Food and Drug Laboratories under World Bank Assisted Capacity Building Project
            </h5>
            <p>
                At the outset no one understood what Environment, Health and Safety was about and how to work to design and put together a health and safety programme. Unfortunately, there is an erroneous impression that since the quantities of chemicals used in the laboratories are small and miniscule, no harm would ensue. This is not right and that is why in most developed countries EHS in the laboratory work is a mandatory activity.            </p>
            <p>
                Aim of the project was to implement Environment, Health and Safety in almost all Food and Drug Laboratories across India.  At the end of each training programme, which was our key activity, many analysts remarked “It was an eye opener”.  If pursued vigorously and rightly, the programme will go a long way in ensuring health and safety of the staff and no serious health effects such as eye injuries, neuropathy, liver and kidney disorders and other illness should occur.
            </p>
            <p>
                During training, our trained Industrial Hygienist visited the laboratories and trained their personnel about ‘DOs’ and ‘DON’T’s’ in laboratories. Analysts/chemists appreciate the nature of harmful health effects following hazardous exposures, and learn to identify the early symptoms and signs of such adverse effects.
            </p>
            <h5>
                Environmental Health Risk Assessment for the Solapur Super Thermal Power Station
               </h5>
            <p>
                In the light of the present study, the results showed that that the main risk for human health may arise from chronic inhalation exposure. Out of total 557 study subjects, maximum no. of individuals were reported with prevalence of Breathing Difficulty (28.2%), and 20.30 % with eye disorders, 19.90 % with wheeze, 18.90 % with chest pain, 16.20 % with frequent abdominal pain, and 15.10 % with skin problems, 14.40 % with diabetes, and 15.30 %, with tuberculosis and 6.30 % with heart diseases 17.60 % with diarrheal disorders respectively.
                </p>
            <p>
                Whatever a few numbers of Cough, chest pain, breathing difficulties and skin problems etc. were reported, could also occur in general population. This area is full of industry like handloom and cotton industry also beedi and tobacco related products are manufactured on large scales As such, the atmosphere round the year in this locality is having dusty & smoke kind of pollutants and thus these diseases among the general public of this locality are not very uncommon.
                </p>
            <p>
                Setting of a coal base thermal power plant may further add to the risk of the existing health disorders. These factors should be looked upon while initiating the proposed power plant set up in the Solapur area.
                </p>
            <p>
                It was heartening to find that even though the bulk of study subjects belonged to low socio economic status, wood and natural gas were easily available. However, prevalence of breathlessness, cough, and wheeze was reported more by Crop residue and wood users. This can be one of the confounding factors for reported respiratory diseases which otherwise might be attributed to fly ash exposure Hypertension, diabetes followed by stroke were the major health problems associated with the population residing in the area nearby the proposed power plant to be set up.
                </p>
            <p>
                Chronic intake of drinking water which may contain high levels of Lead, Mercury, Cadmium, Iron, Arsenic and zinc etc. which may lead to the Increase in the alkalinity and turbidity of the water and thereby causing various abdominal disorders and water borne diseases. In our study, frequent loose motion, gastric discomfort and frequent abdominal pain were associated with the various sources of drinking water i.e. surface water, hand pump, wells, and municipal water.
                </p>
            <p>
                Heavy metals are non-biodegradable and persistent and are known to cause deleterious effects on animal and human health. Both acute and prolonged exposures to heavy metals cause various diseases. Dietary intake of toxic elements is the main route of exposure for most people. Owing to industrialization, heavy metal pollution of soil ecosystems has become topic of concern worldwide and a developing country like India. Sites contaminated with hazardous materials pose serious risks to human health, and the health risk assessment caused by contaminated sites will be the focus of future research.
                </p>
            <p>
                Spot Urine samples were collected from each subject in villages chosen for the study within 10 km area. (The urine was collected for estimating Mercury and Arsenic levels in the study subjects). No significant difference was found with respect to Arsenic and mercury levels in urine samples in the representative 47 subjects with p value being 0.854 and 0.431 for arsenic and mercury.
                </p>
            <p>
                As per the observation from different villages for urine Arsenic and mercury levels in selected individuals it was found that both Mercury and Arsenic levels were well below the permissible reference values.  The good thing that has come out from our study is that the urinary mercury and arsenic levels were within normal limits allaying the fears of environmentalists and others who fear mercury and arsenic pollution in the event of burning huge quantities of coal.
                </p>
            <p>
                Consumption of foodstuff with elevated levels of heavy metals may lead to high level of accumulation in the body causing related health disorders.

                Among different vegetables, garlic showed highest value of MPI followed by potato. As compared to the vegetables, fruits showed lower metal pollution index. Higher MPI of garlic, potato and sugarcane suggests that these vegetables may cause more human health risk due to higher accumulation of heavy metals in the edible portion.
                </p>
            <p>
                To assess the health risk associated with heavy metal contamination of plants grown locally, estimated exposure and risk index were calculated. The results showed that Cd, Pb and As, Hg contamination in plants had greatest potential to pose health risk to the consumer. Health risk index was more than 1 for garlic followed by sugarcane, potato. Fruits crops have lesser concentrations of metals than vegetables, suggesting a lower risk of health risk in the studied population.
                </p>
            <p>
                From overall study it can be revealed that the metal accumulation in vegetable is quiet safe. The MPI, HRI and HI value in some vegetable for some elements are in alarming level and the adverse effect can be reduced by consuming those vegetables in lesser amount. Moreover it is suggested that regular monitoring should be enforced before and after commissioning the Power Plant as these metal accumulation can be toxic to the consumers when they are present in excess or cause different diseases when present in a quantities not suitable for human body.
                </p>

            <h5>
                Environmental Health Risk Assessment for the Talcher Super Thermal Power Station
                </h5>
            <p>
                Human Health Risk Assessment in NTPC’s super thermal power station, at Kaniha, was undertaken to assess and characterize short and long term health risks created by emissions, effluents, and contamination of the soil, if any. Project conceptualized data collection of 600 individuals through an epidemiological study that included interviewer conducted health assessment using questionnaire, measuring height and weight, Blood Pressure, and recording pulmonary functions. For the measurement of heavy metals urine samples were collected. Air quality parameters and water quality testing was undertaken by a NABET approved consultant.
                </p>
            <p>
                Two main purpose of the study were:
                </p>
            <ul>
                <li>
                    To collect, study and analyze mid-term air quality data and water quality data, for identifying specific health risk concerns that required urgent or early action from an emission control perspective.
                    </li>
                <li>
                    The risk assessment in the first instance was based on comparison of measured levels of pollutants in ambient air, water with published national or international standards or guidelines. These standards or guidelines were in turn based on available scientific evidence and factor into formulation potential adverse health end points caused by these substances.
                    </li>
            </ul>
            <p>
                Some key pollutants identified in water were above the level regarded as safe by pollution control authority and standard setting agencies. Fall out of fly ash, source of drinking water, distance from exposure site, period of residence were our focus of concern while conducting this health risk assessment. We analyzed the prevalent disease and secondary data collected from hospitals. There are indications that fly ash containing metals like iron, lead, nickel, arsenic, cadmium might be contaminating drinking water sources.

                Both primary and secondary data were used for the analysis of the risks. The secondary data collected and kept at the NTPC hospital was analyzed. Base line data for human exposure was generated by determining Arsenic and Mercury levels with bio monitoring technique. Health assessment of 6 villages was undertaken around 10 km radius using epidemiological methods. All such data went in to risk estimation and risk characterization at the NTPC power plant.
                </p>
            <p>
                An interview schedule was developed which was pre tested and administered by a team of trained investigators at the health assessment camps specially organized for the project.

                The overall picture of hazards and risks was created. The ambient air monitoring was done upstream and downstream in selected villages around 10 km radius of NTPC. The PM2.5, Mercury, Arsenic, Chromium levels were within limits, and below the prescribed CPCB and USEPA limits. But 90 percent of people complained of fly ash emission. They thought their health problems were due to fly ash fall out near their residence. People reported highest amount of fly ash fallout occurred in the summer season. As fly ash might contain quantity of arsenic and other heavy metals, it might contaminate and be a source of serious pollution of the drinking water. Consumption of drinking water contaminated by fly ash pond might have led to raised levels of arsenic in the humans, as brought out by urinary estimation of the element.

                There was significant affect on health of individuals living within the 4-6 kms radius from plant stack.

                The study also revealed that people working in the fly ash brick manufacturing plant inside NTPC reported joint, back, shoulder pains and cramps. This might be due to improper ergonomic factors or excessive manual load handling. Long workings hours without rest and non use of personal protective equipment during work may also have contributed. Pulmonary function tests done using Spirometer showed that 20 percent workers were having severe pulmonary obstruction, and 33 percent were having moderate to mild obstruction.

                The two common fuel used for cooking were coal and wood. It was heartening to find that even though the bulk of study subjects belonged to low socio economic status, coal and wood were easily available. However, prevalence of breathlessness, cough, and wheeze was reported more by coal and wood users. This can be one of the confounding factors for reported respiratory diseases which otherwise might be attributed to fly ash exposure.

                As per the analysis of secondary data collected from the hospitals, there were 650 cases of Broncho pneumonia and pulmonary bronchitis in Child Emergencies department,

                There was increase in the incidence of cases admitted in ICU with Diabeties and Stroke (CVA) between years 2009-10 - 2011-12. A few publications have suggested a possible link between particulate air pollution and the stroke (CVA). There was a decrease in the incidence of myocardial infarction, Angina, Cerebro vascular diseases, COPD. In all, 76 injury cases were reported in year 2010. Out of 76 cases, 9 people were declared dead. Another 70 injury cases were reported in year 2011, and 4 people were declared dead due to injuries in that year.  A total of 12 injury cases were reported till data in year 2012. A total of 16 Poisoning cases (Pesticides, pharmaceuticals, heavy metals etc) were reported in year 2010. Out of 16 cases, 4 people were dead; 12 poisoning cases were reported in year 2011. Out of 12 cases 1 person died. Only 1 poisoning case was reported in year 2012.

                Finally we tried to estimate total disease burden in the 600 subjects that we studied. Highest proportion of case reported cough (41.3%), followed by 33.3% with gastric discomfort, and 18.5% reported breathing difficulty. In all 19.5% experienced eye disorders, another 19.3% skin problems, and 2.3% jaundice. A total of 8.8% had joint pains, and 4.3%, 4.8% reported abdominal pain and diarrheal disorders respectively. The good news is that the urinary mercury level were within normal limits allaying the fears of environmentalists and others who fear mercury pollution in the event of burning huge quantities of coal.
                </p>
            <h5>
                health effects of long term exposure of aircraft noise on the residents of Dwarka and Vasant kunj
                </h5>
            <p>
                Aim of the study was to find health effects of long term exposure of aircraft noise on the residents of Dwarka and Vasant kunj areas. The main problem addressed in this research was if the aircraft noise was related to an adverse health impact, and if the people exposed to high frequency noise experienced any auditory, non auditory and behavioral effects? The study evaluated the health effects prevalent in aircraft noise exposed areas. The research question related to noise has been given little attention by the previous researchers; hence the study is a baseline for the future reference in this area in India.

                

                The study has given us an in sight into prevailing noise associated problems of the residents in Dwarka and Vasant Kunj area.

                

                The aircraft induced average increase in the noise levels over the background levels were significant. The early effects of noise exposure on health have started appearing. The intermittent but excessive rise in noise levels were found to be significant enough. These may cause health problems in the exposed respondents over a period of time should the levels remain unchanged. The unique feature of this study was to find out association between aircraft noise and prevalence of noise induced health effects.

                

                After adjusting for significant and potential confounding factors, the study revealed that Auditory, Non auditory and Behavioral effects varied significantly with aircraft noise exposure. The study found prevalence of auditory effect like ringing sensation following exposure to aircraft noise. Though the difference between exposure and effect did not show statistical significance, the proportion of subjects reporting ringing sensation was higher in areas with air craft noise. The  non auditory complaints such as increased frequency of headache, gastric discomfort, sleep disturbances, quality of sleep, sleep medication, general tiredness have been found to be significantly prevalent in the airport staff by earlier researchers in other parts. These subjects did find noise causing discomfort which was statistically significant.  This was an important adverse impact of air craft noise.
 
                 

                Hypertension was not that much prevalent, but showed significant variation with the noise levels. Communication interference was found to be insignificant in the exposure group as people tend to get used to prevailing aircraft noise. Regarding perception of noise, respondents showed concern about the noise levels at their workplace. Some of the respondents felt need to decrease the noise levels while others felt comfortable.

                The hypothesis was statistically significant for most of the noise related health effects. Therefore it can be concluded from the study that aircraft noise does affect the health of people in air corridors and more research should be carried out in this area in future after some more time has elapsed. Since environmental health effect is related to the intensity and duration of exposure which leads up to the build up of a cumulative dose causing harm. In that sense, both the duration and intensity of exposure, and the resulting dose (dose is related to concentration X time) in the areas affected by air craft noise has not reached a level to cause Permanent Threshold shift in this situation.
                </p>
            <h5>
                Exploratory Study on Mercury Exposure in Delhi
                </h5>
            <p>
                Study of mercury use and exposure in selected major hospitals in Delhi including the leading dental healthcare institution was undertaken at the behest of Delhi Pollution Control Committee (DPCC) which is the first pollution controlling authority in the country to appreciate the serious public and environmental health risk arising out of the continued use of mercury in healthcare.  In September 2005, the World Health Organization published a policy paper regarding the use of mercury in health care and recommended adopting short, medium and long-term strategies with a view to replacing mercury inputs and devices in the health care sector. Several European countries such as Sweden, Norway, Denmark and France, as well as many states in the United States, have already placed bans on mercury thermometers and other mercury-containing devices in the health care sector. The elemental form of mercury contained in the healthcare equipment on release, gets in to the water bodies where is undergoes a change in speciation to become methyl mercury. The latter is one of the most toxic forms of mercury with disastrous health outcomes for those who experience exposure. Unlike most  developed countries which recognize healthcare work as hazardous, and have established occupational and environmental health units in large hospitals, India is yet to appreciate the health and environmental risks that operations of large hospitals constitute. There is an urgent need for intervention on this front. Study was undertaken in 22 hospitals and 632 subjects were studied. As part of the study measurement of air borne concentrations of mercury was undertaken in Dental health care units by an experienced industrial hygienist at Drexel University School of Public Health, Philadelphia with which the Centre for Occupational and Environmental Health (COEH) at Maulana Azad Medical College has collaboration. The bio monitoring was also undertaken for those having possible exposure to mercury during amalgam preparation to determine the body burden of mercury.
                </p>
            <p>
                In all 97 subjects were examined for the presence of mercury in urine. The plan to have blood mercury levels was shelved as the subjects under study refused to give blood samples. A special interview schedule was developed for the project that was pre tested and provided to the 632 subjects. The analysis of the data revealed that a high proportion of subjects were somewhat aware of the hazards of mercury and its toxicity. However, when it came to the exact target organ and the body systems at risk from mercury exposure, the subjects were not as knowledgeable. Most frequently used devices were the blood pressure devices. This raises our concern threshold as blood pressure instrument contain fairly high amount of mercury, i.e., about 100 gm. Any breakage is likely to release almost a lethal quantity of elemental mercury creating an emergency if the breakage occurred in a closed unventilated place. The knowledge about the speciation of mercury was even poorer. Only 48 per cent correctly identified that healthcare mainly used elemental mercury.
                </p>
            <p>
                The lack of a proper procedure and understanding about the cleanup of spilled mercury did not come as a surprise. However, this revelation makes a strong case for the replacement of mercury containing equipment. This statement is further supported by the fact that there was inadequate knowledge and record keeping when mercury spillage occurred. Only half of the hospitals provided standard operating procedures for clean up. The situation seems to be perilous as inappropriate practices were employed while dealing with spills, i.e., not changing clothes after clean up or preventing people from stepping over spill, not taking cognizance of the state of ventilation and temperature etc. The most serious concern observed was washing down the drain spilled mercury rather than collecting and storing it till suitable disposal could be organized. Most subjects had no clear understanding about the type of proper Personal Protective Equipment (PPE) like gloves used for spill cleanup. The poor work practices employed during a cleanup were further compounded by use of ineffective and incorrect respirators. Only in less than 2 per cent cases was the proper cartridge respirator used. Further, there was confusion about the disposal of spilled mercury. There is no ‘Secure Land fill’ in the city of Delhi which hampers the disposal of mercury containing waste. The most inexcusable practice observed was that of using a broom for clean up or washing the mercury down the drain. The analysis also revealed a significant prevalence of the health effects like confusion, forgetfulness, muscle spasm, and tremors by the respondents. The prevalence of unexplained arthritis, fatigue, and insomnia was also reported and was statistically significant. These results are significant and reported for the first time in India and may be due to exposure to elemental mercury while preparing amalgam. These results are in concordance with the results of similar studies undertaken in developed countries and documented in peer reviewed journals. From the study we conclude that the mercury concentration in Dental workplace in Delhi may be occasionally hovering above the prescribed safe levels of 10 μg/m3. This would create a definite health risk to the dental healthcare workers. Though the bio monitoring did not reveal an excess of body burden except in one case, i.e. above 5μg/l, the study population was young. Since mercury has a bio accumulative nature, it is highly likely that as these workers grow older, they may have a higher body burden of mercury that may lead to a variety of health outcomes. The results of this study support the DPCC initiative to phase out mercury from the health care establishments in Delhi. A national campaign for phasing out mercury and creating awareness is the need of the hour along with declaring all healthcare establishments as hazardous. The environmental, health, and safety protection should be made mandatory with a team of environmental and occupational health specialist, industrial hygienist, chemist, and safety expert participating as far as large hospitals are concerned.
                </p>
        </div>
    );
});


export default Maessage;
