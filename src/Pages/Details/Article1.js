import React, { useEffect, useState } from 'react';



const Article1 = React.memo(function Article1(props) {



    return (
        <div className='paragraphs'>
            <p>
            The course is controlled by the Directorate General Factory Advice Service & Labour Institutes, Government of India, Ministry of Labour & Employment. The course is called as “POST GRADUATE CERTIFICATE COURSE IN INDUSTRIAL HEALTH – ASSOCIATE FELLOW OF INDUSTRIAL HEALTH”. In short, it is written as A.F.I.H. It’s a three months full time regular course.
            The objectives of the course is to enable the doctors –</p>
            <ul>
                <li>
                to identify and manage the occupational health disorders / occupational diseases encountered in various industries in the country and to manage the industrial injuries caused by chemical intoxication, in general and in hazardous process industry in particular.

                </li>
                <li>
                to suggest preventive and control measures of such occupational health problems.

                </li>
                <li>
                to advise, supervise and participate in the national occupational health programmes for the health protection of industrial workers, improving national productivity and national prosperity.

                </li>
                
            </ul>
           <h5>ELIGIBILITY FOR ADMISSION</h5> 
           <p>
           Candidates seeking admission must possess MBBS qualification with full registration of Medical Council of India.

           </p>
           <p>
           Minimum of 1 year experience in industry or relevant field of occupational health or 2 years experience otherwise, after completion of compulsory internship.

           </p>

           <h6>EXAMINATION:</h6> 
            On completion of 3 months curriculum, examination shall be conducted and successful candidates will be awarded the certificates to that effect.
            ELIGIBILITY TO APPEAR IN THE EXAMINATION:
            Candidates must have attended the classes regularly and have 75% attendance. A certificate in this respect must be issued by the Director of the course & accompany the application. (N.B.): Any shortfall in attendance – not less than 60% in any case – may be considered for condonation provided the shortfall is due to self serious sickness & supported by the medical certificate from a competent medical practitioner. However, this genuine circumstances, & every such case shall be treated separately and not as a rule.
            Candidates must have submitted the project work, tutorial/presentation of paper, practical note book and field visit books.

        </div>
    );
});



export default Article1;
