import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>
            <h4>
                From SARS-CoV to Coronavirus Disease 2019 (COVID-19) -A Brief Review
</h4>
            <h5>
                Abstract

</h5>
            <p>
                Emerging and reemerging pathogens are global challenges for public health. For the third time in as many decades, a zoonotic coronavirus has crossed species to infect human populations. Given the Severe Acute Respiratory Syndrome Coronavirus (SARS-CoV) outbreak in 2002 and the Middle East Respiratory Syndrome Coronavirus (MERS-CoV) outbreak in 2012, 2019-nCoV is the third coronavirus to emerge in the human population in the past 18 years-an outbreak that has raised great hue and cry globally as it has sickened thousands of people across the world. This novel coronavirus was named as COVID-19 which stands for Corona Virus Disease 2019. The 2019-nCoV originated from Wuhan, Hubei Province, China beginning in December 2019. The World Health Organization declared it as a Public Health Emergency of International Concern. At that point, there were 9826 confirmed cases globally, with 213 deaths. Of these, 9720 cases were in China; outside of China, health authorities reported 106 confirmed cases in 19 countries. As of February 2, the WHO updated the count, reporting 14 557 confirmed cases from 24 countries-14 411 of them in China. In this review we focus our attention on Covid-19 and how different this epidemic is from the previous two outbreak caused by coronavirus.

</p>
            <h5>
                Introduction
</h5>
            <p>
                Coronaviruses (CoV)  belongs to a group of  RNA viruses that causes disease in mammals and birds. Coronaviruses are common throughout the world, known to cause mild respiratory tract infections being the second most common
                cause of common cold among humans, until the emergence of SARS in 2002 and  MERS in  2012 which caused major outbreak causing  pneumonia like illness.  Now, a  novel coronavirus was identified in Wuhan, China, in December 2019 i.e. Covid-19. The World Health Organization declared
                it as a Public Health Emergency of International Concern. The impact of this outbreak is yet unclear as transmission of the disease is still persisting
<a href="https://www.researchgate.net/publication/340060881_From_SARS-CoV_to_Coronavirus_Disease_2019_COVID-19_-A_Brief_Review" target='_blank'> Read More ...</a>

            </p>

            <h6>Keywords :</h6>
            <p>
            SARS-CoV, COVID-19, Respiratory Syndrome, Coronavirus
</p>

            <h4>
                Assessment of the air quality and its impact on health and environment in India. </h4>
            <p>
                Introduction: The health impact of air pollution caused by firecrackers used during festival time is an area of great concern in Indian population. Trapping of pollutants due to burning of firecrackers during Diwali and Dusshera festivals promotes the formation of smog, which leads to raise in respirable suspended particulate matter (RSPM) and SPM values above the standard values. Aim: This study was designed to assess the air quality pre- and post-Diwali and Dusshera in North India and also to find out the exposure effect of poor air quality on the health of population as well as health risk assessment of the population. Materials and Methods: A total of 470 individuals of different age groups were interviewed during Dusshera festival (i.e., both pre- and post-Dusshera) in four chosen areas. Besides, 788 individuals of different age groups were interviewed during Diwali festival (i.e., both pre- and post-Diwali) in the same four areas. A total of 223 individuals also underwent pulmonary function testing (portable spirometry) on a random basis (fifty in each area, 3 days before and after Diwali). Results: The comparison of respiratory disease complaints pre- and post-Diwali showed that there was a significant increase in the complaints of cough post-Diwali among the participants of Kotla (6.7% vs. 28.9%). However, there was a significant decrease in cough and breathlessness post-Diwali in Parivesh Bhawan. The ambient air quality of three residential areas was within normal limits both pre- and post-Diwali with respect to sulfur dioxide and nitrogen dioxide, but there was an increase in the levels of PM10 and PM2.5 post-Diwali. Conclusion: The level of suspended particles in the air increases alarmingly which can be associated with eye, respiratory, and allergic problems. Crackers and fireworks were found to be the chief sources of air pollution during festivals in India. Even though the impact of Diwali is short term, the short-term exposure of these pollutants in the environment affects the standard values of air particulate and can cause health complications.
   </p>
            <h6>Keywords :</h6>
            <p>
                National Ambient Air Quality Standards, pulmonary function test, respirable suspended particulate matter
</p>
        </div>
    );
});


export default Maessage;
