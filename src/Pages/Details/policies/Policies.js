import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>
            <ul>
                
                <li><a href="http://cpcb.nic.in/displaypdf.php?id=aHdtZC9IV01fUnVsZXNfMjAxNi5wZGY=">Hazardous And Other Waste (Management Transboundary Movement) Rules 2016</a></li>
                <li>
                <a href="http://www.cpcb.nic.in/wast/bioimedicalwast/Rev_Draft_Gdlines_CBWTFs_26022014.pdf">Guidelines for Common Bio-medical Waste Treatment Facilities 2014</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/divisionsofheadoffice/hwmd/bio-medicalrule-2003%28ammendment%29.pdf">Bio-medical Waste (Management &amp; Handling) Amendment Rules, 2003</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/divisionsofheadoffice/hwmd/biomed-Rules-1998.pdf">Bio-Medical Waste (Management and Handling) Rules, 1998</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/wast/bioimedicalwast/Common_Bio_Medical_Waste_treatment_facilities.pdf">Bio Medical Waste (BMW) Treatment Facilities - Guidelines</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/wast/bioimedicalwast/Common_Bio_Medical_Waste_treatment_facilities.pdf">Bio Medical Waste (BMW) Treatment Facilities - Guidelines</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/wast/bioimedicalwast/DesignandconstructionofBMWincinerator.pdf">Design and Construction of Bio Medical Waste(BMW) Incinerator - Guidelines</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/Guidelines_for_ESM_%20MercuryW_fromHCFs.pdf">Environmentally Sound Management of Mercury Waste Generated from Health Care Facilities - Guidelines</a>
                </li>
                <li>
                <a href="http://www.cpcb.nic.in/Guidelines_BMW_UIP.pdf">Management of Bio Medical Waste (BMW) Generated during Universal&nbsp;<span data-scayt_word="Immunisatio" data-scaytid="2">Immunisatio</span>&nbsp;<span data-scayt_word="Programme" data-scaytid="3">Programme</span>)&nbsp;<span data-scayt_word="UIP" data-scaytid="4">UIP</span>&nbsp;- Guidelines</a>
                </li>
               <li>
               <a href="http://egazette.nic.in/WriteReadData/2016/172451.pdf">Notification : Regulation of Lead Contents in Household and Decorative Paints Rules, 2016</a>
               </li>
               <li>
               <a href="http://www.moef.gov.in/sites/default/files/EWM%20Rules%202016%20english%2023.03.2016.pdf">E-Waste (Management) Rule 2016</a>
               </li>
               <li>
               <a href="http://cpcb.nic.in/displaypdf.php?id=cGxhc3RpY3dhc3RlL1BXTV9HYXpldHRlLnBkZg==">Plastic Waste Management (Amendment) Rules, 2018</a>
               </li>
               <li>
               <a href="http://cpcb.nic.in/displaypdf.php?id=aHdtZC9TV01fMjAxNi5wZGY=">Municipal Solid Waste (M &amp; H) Rules 2016</a>
               </li>
            </ul>
         
        </div>
    );
});


export default Maessage;
