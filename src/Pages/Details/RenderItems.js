import React, { useEffect, useState } from 'react';
import * as paths from '../../components/Paths';
import Article from './Article';
import Article1 from './Article1';
import Article3 from './Article3';
import Message from './aboutus/messages';

import Board from './aboutus/board';
import Profile from './aboutus/profile';
import Secretory from './aboutus/secretary';
import Journey from './aboutus/journy'
import Team from './aboutus/team';

import Workshopt from './events/Workshop';
import Seminars from './events/Seminars';
import Conferences from './events/Internation_conference'

import Policies from './policies/Policies'

import Publications from './publications/Poblication'

import ContactUs from './contactus/ContactUs'
import Gallery from './gallery/Gallery'
import Research from './reasearch/research'

const RenderItems = React.memo(function RenderItems(props) {
    switch (props.type) {
        case paths.Afih:
            return <Article1 />;
        case paths.Messages:
            return <Message />;
        case paths.Team:
            return <Team />
        case paths.Board:
            return <Board />
        case paths.Secretary:
            return <Secretory />
        case paths.Profile:
            return <Profile />
        case paths.Journey:
            return <Journey />
        case paths.Conference:
            return <Conferences />
        case paths.Workshops:
            return <Workshopt />
        case paths.seminars:
            return <Seminars />
        case paths.Policies:
            return <Policies />
        case paths.Publications:
            return <Publications />
        case paths.ContactUs:
            return <ContactUs />
        case paths.Gallery:
            return <Gallery />
        case paths.Research:
            return <Research />    
        default:
            return <Article1 />;
    }
});
export default RenderItems;