import React, { useEffect, useState } from 'react';



function App() {



    return (
        <div className='paragraphs'>
            <p>
                Centre for Occupational and Environmental Health (COEH) was set up in 1998. It is one of it’s kind centre set up in any medical college in India. Centre has been working on many fronts such as teaching and training physicians, assisting Govt. as well as other agencies in supervising and monitoring the compliance with environmental laws, conducting research, and working to raise their level of understanding of occupational and environmental health issues.</p>
            <p>
                We work with other groups/agencies around the country as well as internationally in an understanding that this will help bring the experience of the ground to the fore, and lead to a more meaningful articulation of issues.
        Centre has been regularly involved in conducting Bio Medical Waste Management (BMW) Training for para – medicals, undergraduates as well as other Health Care Workers (HCWs). BMW Training is mandatory under the Biomedical Waste Management Rules, 2016 as notified by the Govt. of India.</p>
            <p>
                It’s the only centre in any medical college in India to start Associate Fellow of Industrial Health (A.F.I.H.) course for physicians. It is mandatory A 3 month full time course regulated by DGFASLI (Director General of Factory Advisory Services & Labour Institutes, Govt. of India). As per Factories Act, this is a mandatory qualification for doctors working in hazardous industry. Other industries also prefer to hire AFIH qualified doctors. This qualification is recognized for appointment as Factory Medical Officer all over India.</p>

        </div>
    );
}



export default App;
