import React, { useEffect, useState, } from 'react';
import { Input, TextField, TextareaAutosize, Button } from '@material-ui/core'



 
class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFormFilled: false
        }
    }
    submitForm = () => {
        this.setState({ isFormFilled: true })
    }

    render() {
        return (
            <React.Fragment>
                {this.state.isFormFilled == false ?
                    <React.Fragment>
                        <div>
                            <TextField
                                style={{ width: 300 }}
                                size="small"
                                label="Your Name"
                                variant="outlined"
                            />
                        </div>
                        <div>
                            <TextField
                                style={{ width: 300 }}
                                size="small"
                                label="Email"
                                variant="outlined"
                            />
                        </div>
                        <div>
                            <TextField
                                style={{ width: 300 }}
                                size="small"
                                label="Phone"
                                variant="outlined"
                            />
                        </div>
                        <div>
                            <TextareaAutosize
                                style={{ width: 300 }}
                                aria-label="minimum height" rowsMin={3} placeholder="Leave your message"
                            />
                        </div>
                        <div onClick={()=>this.submitForm()}>
                            <Button   variant="contained" color="primary">
                                Submit
    </Button>
                        </div>

                    </React.Fragment>
                    : <div>
                        <h5>Thanks, We will contact you soon.</h5>
                    </div>
                }
            </React.Fragment>

        );
    }

}



export default Form;
