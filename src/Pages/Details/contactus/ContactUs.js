import React, { useEffect, useState, } from 'react';
import { Input, TextField, TextareaAutosize, Button } from '@material-ui/core'

import Form from './ContactForm'


 function Maessage(props) {
    return (
        <div className='paragraphs'>
            <h5>
                You can contact us at:
          </h5>
            <h6>
                Centre For Occupational & Environmental Health
        </h6>
            <p>
                BL Taneja Block, Ground Floor, Lok Nayak Hospital, New Delhi - 110002
        </p>
            <h6>
                Phone : +91-011-23233519, 23214731
        </h6>
            <h6>
                email : info@coeh.org.in
        </h6>
          <Form />
        </div>
    );
};


export default Maessage;
