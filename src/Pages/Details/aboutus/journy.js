import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>
            <h5>About COEH</h5>
            <p>
                Centre for Occupational and Environmental Health (COEH) was set up in 1998. Initially the centre was providing services to the Department of Health and Family Welfare, Government of National Capital Territory of Delhi. Subsequently, the department started assisting the department of Environment and Delhi Pollution Control Committee (DPCC) for supervising, and monitoring of bio medical waste management and other environmental health problems..
            </p>
            <h5>Establishment</h5>
            <p>
                To give it s flexibility, an autonomous body by the name of Indraprastha Vyavsayik Evam Paryavarneeya Swasthya Samiti (IVPSS), registered under The Societies Registration Act, 1860. Hon’ble Minister of Health and Family, Govt. of NCT of Delhi is the President of the Society and Principal Secretary (H&FW), GNCTD is the Chairperson of the Executive Committee.
            </p>
            <h5>Work</h5>
            <p>
                The Centre has been working on many fronts, such as teaching and training physicians, assisting various Govt. as well as other agencies in supervising and monitoring the compliance with environmental laws, conducting research, and working to raise their level of understanding of occupational and environmental health issues.
            </p>
        </div>
    );
});


export default Maessage;
