import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>
            <div>
                <h5>Message From Dean</h5>
                <div className="flexibleRow">
                    <p>
                        “It’s been pleasure associating with Centre for Occupational and Environmental Health as it’s Academic Head. The Department of Occupational and Environmental Health is concerned with the health effects of exposures to various pollution viz., air, water, noise, soil etc., pesticides, organic solvents, dusts and physical hazards, which occur in the environment, the home or the workplace. The department draws from the knowledge generated from disciplines that contribute to recognizing, assessing, and controlling these risks that include epidemiology, toxicology, microbiology, safety engineering, industrial hygiene, medicine, statistics and many other fields. The department includes a multidisciplinary core faculty and a large adjunct faculty. Our honorary faculty includes both national and international experts. The constituents of MAMC – the College, Lok Nayak Hospital, GB Pant Institute of Postgraduate Medical Education and Research (formerly G.B. Pant Hospital), Guru Nanak Eye Centre, and Maulana Azad Institute of Dental Sciences co-exist on one campus which provides an extraordinary environment for training and research.
                        The mission of the centre is to create a safe workplace and environment for different stakeholders.
                        I look forward to the continued support of each member of the Centre so that we as a team can live our dreams and hopes.

                       <b> – Dr. Shushil Kumar
                           </b>
                    </p>
                    <div style={{ paddingLeft: 50, paddingRight: 50 }}>
                        <img src={require('../../../assets/dean.jpg')} />
                        <h6 className='text-center'>Dr. Shushil Kumar</h6>
                        <h6 className='text-center'>ACADEMIC HEAD, COEH</h6>
                    </div>

                </div>

            </div>
            <div>
                <h5>Message From Member Secretary</h5>
                <div className="flexibleRow">
                    <p>
                        Centre for Occupational & Environmental Health at Maulana Azad Medical College was set up to address the growing concerns and hazards related to environment and health. With rising pollution and occupational hazard at work and in community, the health professionals have an onerous responsibility to inform policy makers. The centre is collaborating in its research activities with many agencies. Besides, carrying out research, the centre brings out a news bulletin on occupational and environmental health, and undertakes studies to generate urgently needed data on adverse impact of environmental pollution. The centre is also training medical professionals in hospital waste disposal and occupational related illnesses. To fulfill its objectives and attain the goals, the Centre functions through a Govt. society known as Indraprastha Vyavsayik Evam Paryavarneeya Swasthaya Samiti (IVPSS) which gives more flexibility and autonomy to the centre to speedily execute proposed plan of action. We all will witness a new and expanded department of Occupational and Environmental Health in the near future.

                        <b> – Dr. Mradul Kumar Daga
                           </b>
                    </p>
                    <div style={{ paddingLeft: 50, paddingRight: 50 }}>
                        <img src={require('../../../assets/dagaji.jpg')} />
                        <h6 className='text-center'>Dr. M.K. Daga</h6>
                        <h6 className='text-center'>MEMBER SECRETARY, IVPSS</h6>
                    </div>
                </div>

            </div>

        </div>
    );
});


export default Maessage;
