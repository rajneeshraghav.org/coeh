import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>

            <ul>
                <li>
                    <h3>Dr. Mradul Kumar Daga, Member Secretary/IVPSS</h3>
                    <div className='flexibleRow'>
                        <div>


                            <p>
                                <b>Dr. Mradul Kumar Daga </b> MD (Medicine), FCCP, FICP, FRCP, FACP is the Director – Professor in the Department of Medicine at Maulana Azad Medical College. He is also the In charge of Centre for Occupational and Environmental Health. Dr. Daga has worked for wide range of Governmental organizations and has clinical experience of more than 35 years. He is intensively working in the interest of public health by attending Out Patient Department (OPD), emergency and hospital services. His role also includes teaching both graduates and post – graduates.
</p>
                            <p>
                                Dr. Daga is one of the renowned chest – specialists of India. He is actively involved in research on air – pollution, water – pollution, e-waste and various other occupational and environmental hazards. He is also Principal Investigators to various successfully completed drug trials.
</p>
                            <p>
                                Dr. Daga is also the Member Secretary of Institutional Ethics Committee and Society for Promotion of Medical Research, Maulana Azad Medical College. He is also on the editorial board of Davidson’s Textbook of Medicine (Last five editions), Lung India Journal, Journal of Medical Specialties, Editorial board of Journal Respiratory Research, Textbook of Family Medicine (Published by IMA) to name a few. He has contributed to around 142 indexed and 80 non – indexed publications. He participated, presented papers and delivered talks in many nNational as well as International conferences. He is recipient of many prestigious awards like State Award to Service Doctors for the year 2010-11.
</p>
                        </div>
                        <div>
                            <img className='teamImage' src={require('../../../assets/team/mkdaga.jpg')} />
                        </div>
                    </div>


                </li>

                <li>
                    <h4>Manish Kumar Jha, M.Sc. (Microbiology),Research Associate</h4>
                    <div className='flexibleRow'>


                        <div>
                            <p>Manish Kr. Jha is Post - graduate in Microbiology. He has immense knowledge of performing various laboratory tests like PCR, ELISA etc. Has also experience of applying methodologies to carry out various experiments as well as developing specimens and samples as well as operating standard laboratory equipment. </p>

                            <h6>

                                Manish Kr. Jha has worked as Project Co-ordinator for various drug trials viz.,

    </h6>

                            <ul>
                                <li>
                                    a)	“Phase III, SCARLET-1 “A Randomized Double-Blind, Placebo-Controlled, Phase IV Study to Assess the Safety and Efficacy/Effects of ART-123 on Subjects with Sepsis and Disseminated Intravascular Coagulation” (Aasai Kesai Pharma);
 </li>
                                <li>
                                    b)	“A prospective, Double Blind, Multicentric, Randomized Phase IV clinical study to compare the efficacy and safety of intravenous Ulinastatin versus placebo along with standard supportive care in subjects of severe sepsis” (Bharat Serums & Vaccine Ltd.);
 </li>
                                <li>
                                    c)	Phase III, SCARLET-2 “A Randomized Double-Blind, Placebo-Controlled, Phase IV Study to Assess the Safety and Efficacy/Effects of ART-123 on Subjects with Sepsis and Disseminated Intravascular Coagulation” (Aasai Kesai Pharma).
 </li>
                            </ul>
                        </div>
                        <div>
                            <img className='teamImage' src={require('../../../assets/team/manishji.jpeg')} />
                        </div>

                    </div>


                </li>

                <li>
                    <h4>AMRAPALI KUNWAR, M.Sc. (Biotechnology),Junior Research Fellow</h4>
                    <div className='flexibleRow'>
                        <div>

                            <p>
                                Amrapali Kunwar has an experience of working with distinguished people. She has worked on the project “Molecular characterization of Groundnut bud necrosis virus (Tospovirus)” and “Analytical microbial techniques used for anticancer drugs- Carfilzomib drug”  under the stewardship of Prof. Jawaid A. Khan Jamia Millia Islamia (Central University New Delhi) and  Dr. Ashish Anand Gupta (Fresenius kabi) respectively. Being a young talent, she is keen to learn and work on diverse nature of research.
 </p>
                            <p>
                                As Junior Research Fellow with the centre, she has worked on two studies viz.,  “Atmospheric Pollutants and their Impacts on Public Health Risk Assessment in Delhi/NCR” awarded and funded by Indian Oil Corporation and Five year Co – hort on “Health impact of firecrackers bursting during Diwali in national capital, Delhi” awarded and funded by Central Pollution Control Board.
</p>
                            <p>
                                She has deep interest on the topic of e-waste and trying to explore her research work on “Chromosome aberration and DNA damage on human population due to E-waste exposure”.
</p>
                        </div>
                        <div>
                            <img className='teamImage' src={require('../../../assets/team/Amrapali.jpeg')} />
                        </div>


                    </div>



                </li>

                <li>
                    <h4>SWEETY MEHRA, M.Sc. (Biotechnology),Junior Research Fellow</h4>
                    <div className='flexibleRow'>
                        <div>
                            <p>
                                Sweety Mehra is a Junior Research Fellow at Centre for Occupational and Environmental Health (COEH). Prior joining to COEH, she has worked on project “Shelf life study of fish at -18 degree” under the guidance of Mrs. Swati Mehta, Hygiene Manager, The Taj Mahal Hotel, New Delhi. She has also done a short project “A study on Mental Health (depression and stress)” under Mrs. Monika Mor. Being a passionate learner, she is always enthusiastic to innovate, learn and work on new ideas.
 </p>
                            <p>
                                As Junior Research Fellow with the centre, she is contributed whole heartedly to two studies namely “Atmospheric Pollutants and their Impacts on Public Health Risk Assessment in Delhi/NCR” awarded and funded by Indian Oil Corporation and Five year Co – hort on “Health impact of firecrackers bursting during Diwali in national capital, Delhi” awarded and funded by Central Pollution Control Board.

</p>
                            <p>
                                Her topics of interests are “Water pollution” and “Risks and Hazards associated with Landfills”. She is continuing her forthcoming research work on “Health Risks Caused Due to Exposure from Landfills”.
</p>
                        </div>
                        <div>
                            <img className='teamImage' src={require('../../../assets/team/Sweety.jpeg')} />
                        </div>
                    </div>



                </li>




            </ul>
            <div className='flexibleRow'> 
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Alice.jpeg')} />
                    <h6>Alice Jacob </h6>
                    <p>Lab Technician</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Nitiji.jpg')} />
                    <h6>Niti Mendiratta </h6>
                    <p>Stenographer</p>
                </div>

                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Ajay.jpg')} />
                    <h6>Ajay Sharma</h6>
                    <p>Accounts Clerk</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Ramesh.jpg')} />
                    <h6>Ramesh S. Nayal</h6>
                    <p>MTS</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Nayal.jpg')} />
                    <h6>Rajender S. Nayal</h6>
                    <p>MTS</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Rajender.jpg')} />
                    <h6>Rajender Joshi</h6>
                    <p>MTS</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Prem.jpg')} />
                    <h6>Prem Singh </h6>
                    <p>Driver</p>
                </div>
                <div className='otherStaff'>
                    <img className='otherteamImage' src={require('../../../assets/team/Puran.jpg')} />
                    <h6>Puran S. Bisht </h6>
                    <p>Peon</p>
                </div>
            </div>

        </div >
    );
});


export default Maessage;
