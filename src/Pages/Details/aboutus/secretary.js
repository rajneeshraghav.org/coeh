import React, { useEffect, useState, } from 'react';




const Maessage = React.memo(function Maessage(props) {
    return (
        <div className='paragraphs'>

            <h3>EXPERTISE - OCCUPATIONAL & ENVIRONMENTAL MEDICINE</h3>

            <h5>Fellowship:</h5>
            <h6>Collegium Ramazzini, </h6>
            <h6>FFOM (RCP, London)</h6>
            <h6>Membership:</h6>

            <p>
                Central Council of Health, Ministry of Health and Family Welfare, GOI, Central Pollution Control Board and Delhi Pollution Control Committee Boards; Former Adviser Environmental Health, Ministry of Environment and Forests and Climate Change, GOI.
                Commissioner, Silicosis – Hon’ble Supreme Court of India,
    </p>
            <h6>
                Achievements:
    </h6>
            <p>
                First Centre of Occupational and Environmental Health, Maulana Azad Medical College, New Delhi
                Invited by ILO, Geneva to finalize Global list of Occupational Diseases
                Chairman of National Bio Medical Waste Committee of CPCB,
                Chairman UNIDO-MoEFCC Project on Bio Medical Waste Management
                Expert Reviewer, UK-India Research on Air Pollution, Biotechnology Dept, GOI,
                On ICMR Committee for Research, NIOH, NIREH
</p>

            <h6>
                Awards:
</h6>
            <p>
                Research Integrity Award, International Society for Environmental Epidemiology
                Irving Selikoff Award, Exemplary contribution to OHS in India,
                Visiting Professor in Occupational Health, Dornsife School of Public health, Philadelphia
                Former Advisor to WHO, SEARO, Chemical Safety
                Was Member of Expert Committee on Occupational and Environmental Health, ILO, Geneva, WHO, Geneva, IARC (WHO, Lyon, France),
                Was Consultant of Environmental health, World Bank Project, Ministry of Health and Family Welfare, GOI.
</p>

            <h6>
                Research:
</h6>
            <p>
                First Study on fire Fighters Occupational Hazards
                First Study of Mercury Exposure, Dental Physicians
                Presentation at the First Occupational Injury Research Symposium (NIOSH), US
</p>
            <h6>
                Advised:
</h6>
            <p>
                Tata Steel, SAIL, Central Electronics Limited, TERI, ESIC.
                Organized training for Physicians in India in Collaboration with UCSF, Harvard, Berkeley, Drexel, and Queensland Universities.
                Advised NGOs in Malaysia, Thailand, and India on Environmental Health.
                Made Delhi Health Care Mercury free, the First City in South Asia to have this distinction
                Scores of Presentations(Scientific meetings) :- US, Canada, Mexico, Russia, UK, Belgium, France, Denmark, Sweden, Finland, Spain, Italy, Greece, Korea, Japan, Australia, Malaysia, Thailand, Indonesia, Sri Lanka, and Nepal.

</p>

        </div>
    );
});


export default Maessage;
