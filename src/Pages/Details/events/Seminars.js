import React, { useEffect, useState } from 'react';



const Article1 = React.memo(function Article1(props) {



    return (
        <div className='paragraphs'>
            <h5>LIST OF SEMINARS</h5>
            <ul>
                <li>
                    Training workshop on “Pesticide, health and Safety” was organized on 29 – 30 Nov. 2002 in collaboration with University of California, USA, Fogarty Foundation USA and Hebrew University, Israel.
                </li>
                <li>
                    Training programme on Improving Productivity through health and safety in cold strorages (Cold Working Environment) held on 18th August 2000 in collaboration with Inspectorate of Factories, Department of Labour, Govt. of NCT of Delhi
               </li>
                <li>
                    A short course on Introduction to Industrial Hygiene was conducted on 26 February 2000.
                    Industrial Hygiene Course organized in
                </li>

                <li>
                    Industrial Hygiene Course organized in collaboration with University of Queensland was held from 19 – 26 February 2000
                </li>

                <li>
                    National Training Course on Occupational and Environmental Epidemiology was organized in collaboration with Centre for Occupational and Environmental Health, University of California, Berkeley in Nov. 1999
                </li>

                <li>
                    National Training Programme on Occupational and Environmental Medicine from 9 – 14 October 2000.
                </li>

                <li>
                    4th Indo – Australian Training Programme on Occupational Environmental Epidemiology from 4 – 12 April 2001 was conducted.
               </li>

                <li>
                    Training workshop on “Pesticide, Health and Safety” from 6 – 7 Nov. 2001 in collaboration with University of California, Davis, USA, Fogarty Foundation, USA and Ministry of Agriculture, GOI
               </li>

            </ul>

        </div>
    );
});



export default Article1;
