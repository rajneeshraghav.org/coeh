import React, { useEffect, useState } from 'react';



const Article1 = React.memo(function Article1(props) {



    return (
        <div className='paragraphs'>
            <h5>LIST OF WORKSHOPS</h5>
            <ul>
                <li>
                    Workshop on “Work Related Illness and its Management” on 25 – 26 August 2005.
                </li>
                <li>
                    Workshop on “Recent Advances in Occupational and Environmental Health” on 1 – 2 April 2005
                </li>
                <li>
                    Workshop on “Recent Trends in Occupational and Environmental Medicine” on 26 February 2005 in collaboration with Directorate of Health Services, GNCTD.
                </li>
                <li>
                    Workshop on “Risk Assessment at Workplace” on 7 Aug. 2004 in collaboration with Deptt. Of Labour, Govt. of NCT of Delhi and Society for the Advancement of Occupational and Environmental Health (SAOEH).
                </li>
                <li>
                    Workshop on “Bio Medical Waste Management” held on 13 April 2004.
                </li>
                <li>
                    Workshop on “Research Methods in Clinical Epidemiology” in collaboration with Clinical Epidemiology Unit at Maulana Azad Medical College held on 24 March 2004.  </li>
                <li>
                    Two day workshop on “Hearing Conservation in Industry” in collaboration with Deptt. Of ENT, MAMC from 6 – 7 2004. </li>
                <li>
                    Workshop on Occupational Safety and Health on 16 – 17 January 2004 in collaboration with Hebrew University School of Public Health and Community Medicine, Department of Occupational and Environmental Medicine and SAOEH.  </li>
                <li>
                    Workshop on Fogarty International Research and Training programme on Environmental and Occupational Epidemiology was conducted from 1 – 12 December 2003.
                    </li>

            </ul>

        </div>
    );
});



export default Article1;
