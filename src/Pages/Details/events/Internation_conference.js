import React, { useEffect, useState } from 'react';



const Article1 = React.memo(function Article1(props) {



    return (
        <div className='paragraphs'>
              <h5>LIST OF CONFERENCES</h5> 
            <ul>
                <li>
                International conference on “Hazards of Asbestos Exposure” in India’ on 3rd December 2006 in collaboration with DGFASLI, Ministry of Labour, Govt. of India and Collegium Ramazzini
                </li>
                <li>
                International Conference from 22- 24 March, 2011 on “Emerging Trends in Preventing Occupational Respiratory Diseases (Silicosis etc) and Cancers in the Workplace”
                </li>
                <li>
                International Meet on Climate, the Workplace and the Lungs” held on 6-8 December 2012
                </li>
                
            </ul>
         
        </div>
    );
});



export default Article1;
