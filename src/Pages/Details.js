import React, { useEffect, useState } from 'react';
import * as paths from '../components/Paths'
import firebase from '../firebase';

import RenderItems from './Details/RenderItems';


function App(props) {

    const [type, setType] = useState(null);

    useEffect(() => {
        console.log("change path")
        let url = window.location.pathname;
        console.log(url)
        setType(url);
       
    }, [window.location.pathname])


    return (
        <div style={{ padding: 100 }}>
            <RenderItems type={type} />
        </div>
    );
}

export default App;
