import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,withRouter
} from "react-router-dom";
import Header from './components/Header'
import Footer from './components/Footer'

import { CircularProgress } from '@material-ui/core'

const Home = React.lazy(() => import('./Pages/Home'));
const Details = React.lazy(() => import('./Pages/Details'));

const CustomRoute = () => {
    return (
            <React.Suspense
                fallback={
                    <CircularProgress />
                }
            >    <Header />
          

                            <Switch>
                                <Route strict exact path="/" component={Home} />
                                <Route exact  path="/details/:link" component={withRouter(Details)} />
                            </Switch>      
          
                <Footer />
            </React.Suspense>

     
    )
}
export default CustomRoute;